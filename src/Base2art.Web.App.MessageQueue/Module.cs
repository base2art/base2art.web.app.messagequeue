namespace Base2art.Web.App.MessageQueue
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Base2art.MessageQueue;
    using Base2art.MessageQueue.Management;
    using Base2art.MessageQueue.Storage;
    using Configuration;

    public class Module : IModule
    {
        private readonly TimeSpan delay;
        private readonly TimeSpan interval;
        private readonly QueueType? queueType;

        public Module(TimeSpan? delay, TimeSpan? interval, QueueType? queueType)
        {
            this.queueType = queueType;
            this.delay = delay.GetValueOrDefault(TimeSpan.FromSeconds(10));
            this.interval = interval.GetValueOrDefault(TimeSpan.FromSeconds(5));
        }

        public IReadOnlyList<IEndpointConfiguration> Endpoints => new IEndpointConfiguration[0];

        public IReadOnlyList<ITypeInstanceConfiguration> GlobalEndpointAspects => new ICallableMethodConfiguration[0];

        public IReadOnlyList<ITaskConfiguration> Tasks => new ITaskConfiguration[]
                                                          {
                                                              new CustomTaskConfiguration(this)
                                                          };

        public IReadOnlyList<ITypeInstanceConfiguration> GlobalTaskAspects => new ICallableMethodConfiguration[0];

        public IHealthChecksConfiguration HealthChecks => null;

        public IApplicationConfiguration Application => null;

        public IReadOnlyList<IInjectionItemConfiguration> Injection
            => new[]
               {
                   this.GetBindedType(),

                   new CustomInjectionItemConfiguration<IMessageQueue, SimpleMessageQueue>(),

                   new CustomInjectionItemConfiguration<IMessageHandler, TaskExecutionMessageHandler>(),
                   new CustomInjectionItemConfiguration<IMessageHandler, RecycleAppHandler>()
               };

        public IReadOnlyList<ICorsItemConfiguration> Cors => new ICorsItemConfiguration[0];

        private IInjectionItemConfiguration GetBindedType()
        {
            var type = this.queueType.GetValueOrDefault(QueueType.InMemory);
            switch (type)
            {
                case QueueType.InMemory:
                    return new CustomInjectionItemConfiguration<IMessageQueueStore, InMemoryMessageQueueStore> {IsSingleton = true};
                case QueueType.FileSystem:
                    return new CustomInjectionItemConfiguration<IMessageQueueStore, FileSystemMessageQueueStore> {IsSingleton = true};
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private class CustomTaskConfiguration : ITaskConfiguration
        {
            private readonly Module mod;

            public CustomTaskConfiguration(Module mod) => this.mod = mod;

            public Type Type { get; } = typeof(MessageHandlerService);
            public IReadOnlyDictionary<string, object> Parameters { get; } = new Dictionary<string, object>();
            public IReadOnlyDictionary<string, object> Properties { get; } = new Dictionary<string, object>();
            public MethodInfo Method { get; } = typeof(MessageHandlerService).GetMethod(nameof(MessageHandlerService.ProcessItem));
            public HttpVerb Verb { get; } = HttpVerb.Post;
            public string Url { get; } = "tasks/queue/process-item";
            public IReadOnlyList<ITypeInstanceConfiguration> Aspects { get; } = new List<ICallableMethodConfiguration>();
            public string Name { get; } = "queue/process-item";
            public TimeSpan Delay => this.mod.delay;
            public TimeSpan Interval => this.mod.interval;
        }

        public class CustomInjectionItemConfiguration<TInterface, TConcrete> : IInjectionItemConfiguration
            where TConcrete : TInterface
        {
            public Type RequestedType { get; } = typeof(TInterface);
            public Type FulfillingType { get; } = typeof(TConcrete);
            public IReadOnlyDictionary<string, object> Parameters { get; set; } = new Dictionary<string, object>();
            public IReadOnlyDictionary<string, object> Properties { get; set; } = new Dictionary<string, object>();
            public bool IsSingleton { get; set; }
        }
    }
}