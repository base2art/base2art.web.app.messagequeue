namespace Base2art.Web.App.MessageQueue
{
    using System.Threading.Tasks;
    using Base2art.MessageQueue;
    using LifeCycle;
    using Web.Messages;

    public class RecycleAppHandler : MessageHandlerBase<RecycleAppMessage>
    {
        private readonly IApplication application;

        public RecycleAppHandler(IApplication application)
        {
            this.application = application;
        }

        protected override Task HandleMessage(RecycleAppMessage message)
        {
            this.application.Reload();
            return Task.CompletedTask;
        }
    }
}