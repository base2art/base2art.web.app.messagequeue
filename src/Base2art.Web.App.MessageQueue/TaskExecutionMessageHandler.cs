namespace Base2art.Web.App.MessageQueue
{
    using System.Threading.Tasks;
    using Base2art.MessageQueue;
    using Web.App.LifeCycle;
    using Web.Messages;

    public class TaskExecutionMessageHandler : MessageHandlerBase<TaskExecutionMessage>
    {
        private readonly IApplication application;

        public TaskExecutionMessageHandler(IApplication application)
        {
            this.application = application;
        }

        protected override Task HandleMessage(TaskExecutionMessage message)
        {
            return this.application.ExecuteJobWithParameters(message.TaskName, message.Data);
        }
    }
}