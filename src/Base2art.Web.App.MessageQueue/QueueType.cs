namespace Base2art.Web.App.MessageQueue
{
    public enum QueueType
    {
        InMemory,
        FileSystem
    }
}